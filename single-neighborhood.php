<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<?php get_template_part( 'template-parts/featured-image' ); ?>

<div class="fullWidth">
	<div class="listings-search-bar">
		<div class="search-bar-outer">
			<div class="search-bar">
				<h2>Property Search</h2>
				<?php dynamic_sidebar( 'home-search-bar' ); ?>
			</div>
		</div>
	</div>
</div>

<div id="page-edge-bleed" role="main">
	<article class="main-content">
	<?php do_action( 'foundationpress_before_content' ); ?>

	<?php while ( have_posts() ) : the_post(); ?>
		<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<header>
				<?php
					$parent_id = wp_get_post_parent_id( get_the_id() );
					if($parent_id > 0) {
						$parent_title = get_the_title($parent_id);
						echo '<a class="back-to-parent" href="'.get_permalink($parent_id).'">back to ' .$parent_title.' area</a>';
					}
				?>
				<h1 class="entry-title"><?php the_title(); ?></h1>
			</header>
			<?php do_action( 'foundationpress_post_before_entry_content' ); ?>
			<div class="entry-content">
			    <ul class="tabs" data-tab>
				  <li class="tab-title active"><a href="#info-tab">Area Information</a></li>
				  <li class="tab-title"><a href="#map-tab">Listing Map</a></li>
				</ul>
				<div class="tabs-content">
				  <div class="content active" id="info-tab">
				    <?php
					    the_content();
				    ?>
				  </div>
				  <div class="content" id="map-tab">
				  	<?php
					  	$options = get_post_meta(get_the_id());

					  	$area_link = '';

						if(isset($options['_fls_area'][0]) && !empty($options['_fls_area'][0])) {
							$area_link =  $options['_fls_area'][0] . '-area/';
						}
						if(isset($options['_fls_area_search_string'][0]) && !empty($options['_fls_area_search_string'][0])) {
							$area_link .=  trailingslashit( $options['_fls_area_search_string'][0] );
						}
						if(!empty($area_link)) {
							$area_link .= 'static_map';
						}

				  	$args = array();

				  	if(!empty($area_link)) {
					  	$args['search'] = $area_link;
					  	$args['search'] .= '/map-view/';
					  	echo mls_search_listings( $args );
				  	}

					?>
				  </div>
				</div>
				<?php
					$post_id = get_the_id();
					$parent_id = wp_get_post_parent_id( $post_id );
					$args = array(
							'post_type' => 'neighborhood',
							'posts_per_page' => -1,
							'post_status' => 'publish',
							'order' => 'ASC',
						);
					if($parent_id == 0) {
						$title = get_the_title();
						//get children of current
						$args['post_parent'] = $post_id;
					}
					else {
						$title = get_the_title($parent_id);
						$args['post_parent'] = $parent_id;
					}

					$the_query = new WP_Query( $args );
					if ( $the_query->have_posts() ) {
						echo '<h4>Neighborhoods in '. $title .'</h4>';
						echo '<ul class="sub-neighborhoods">';
						while ( $the_query->have_posts() ) {
							$the_query->the_post();
							echo '<li><a href="'.get_permalink().'"';
							if(get_the_id() == $post_id) echo ' class="active" ';
							echo '>' . get_the_title() . '</a></li>';
						}
						echo '</ul>';
					}

					/* Restore original Post Data */
					wp_reset_postdata();
				?>
			</div>
			<footer>
				<?php wp_link_pages( array('before' => '<nav id="page-nav"><p>' . __( 'Pages:', 'foundationpress' ), 'after' => '</p></nav>' ) ); ?>
				<p><?php the_tags(); ?></p>
			</footer>
	</article>
	<?php get_sidebar(); ?>

</div>

<?php get_footer();
