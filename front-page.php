<?php
/**
 * The home page file
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0
 */

get_header(); ?>

<div class="home">
	<div class="row fullWidth">
		<div class="home-top">
			<?php dynamic_sidebar( 'home-top' ); ?>
		</div>
	</div>

	<div class="row searcharea">
		<div class="medium-12 large-10 large-centered columns">
			<div class="search-bar-outer">
				<div class="search-bar">
					<?php dynamic_sidebar( 'home-search-bar' ); ?>
				</div>
			</div>
		</div>
	</div>

	<div class="row fullWidth featured">
		<h2 class="group-title"><span>Featured Properties</span></h2>
		<?php dynamic_sidebar( 'home-featured' ); ?>
	</div>

	<div class="fullWidth area-info">
		<div class="row">
			<h2 class="group-title text-center"><span>Get to know Jackson</span></h2>
			<article class="columns widget">
				<a href="/getting-to-know-jackson/area-map/" >
					<img width="600" height="437" alt="Area Map - The Town" src="/wp-content/uploads/2016/11/Area-The-Town-home.jpg">
					<h4>Area Map / The Town</h4>
					<div class="special-hover-overlay image-1"></div>
				</a>

			</article>
			<article class="columns widget">
				<a href="/getting-to-know-jackson/recreation/" >
					<img width="600" height="398" alt="Recreation" src="/wp-content/uploads/2016/11/Recreation-home.jpg">
					<h4>Recreation</h4>
					<div class="special-hover-overlay image-2"></div>
				</a>

			</article>
			<article class="columns widget">
				<a href="/news-events/" >
					<img width="600" height="400" alt="News &amp; Events" src="/wp-content/uploads/2016/11/News-Events-home.jpg">
					<h4>News &amp; Events</h4>
					<div class="special-hover-overlay image-3"></div>
				</a>

			</article>
		</div>
	</div>

	<div class="sub-footer">
		<div class="row fullWidth about">
			<div class="small-12 medium-6 large-6 columns">
				<div class="sub-footer-left">
					<?php dynamic_sidebar( 'sub-footer-1' ); ?>
				</div>
			</div>
			<div class="small-12 medium-6 large-6 columns">
				<div class="sub-footer-right">
					<?php dynamic_sidebar( 'sub-footer-2' ); ?>
				</div>
			</div>
		</div>
	</div>

</div>

<?php get_footer(); ?>
