<?php
/**
 * Custom Functions
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

// Add Gravity Forms Visibility Settings
add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

// Update Read More on Excerpts
function wpdocs_excerpt_more( $more ) {
    return sprintf( '<a class="read-more" href="%1$s">%2$s</a>',
        get_permalink( get_the_ID() ),
        __( 'Read More', 'textdomain' )
    );
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );

// Decrease Excerpt length to 30 words
function wpdocs_custom_excerpt_length( $length ) {
    return 30;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );

//cmb2
require_once get_stylesheet_directory() . '/library/cmb2/init.php';

//registers "neighborhood" post type
$labels = array(
		'name'               => _x( 'Neighborhoods', 'neighborhood', 'varsha' ),
		'singular_name'      => _x( 'Neighborhood', 'neighborhood', 'varsha' ),
		'menu_name'          => _x( 'Neighborhoods', 'admin menu', 'varsha' ),
		'name_admin_bar'     => _x( 'Neighborhood', 'add new on admin bar', 'varsha' ),
		'add_new'            => _x( 'Add New', 'neighborhood', 'varsha' ),
		'add_new_item'       => __( 'Add New Neighborhood', 'varsha' ),
		'new_item'           => __( 'New Neighborhood', 'varsha' ),
		'edit_item'          => __( 'Edit Neighborhood', 'varsha' ),
		'view_item'          => __( 'View Neighborhood', 'varsha' ),
		'all_items'          => __( 'All Neighborhoods', 'varsha' ),
		'search_items'       => __( 'Search Neighborhoods', 'varsha' ),
		'parent_item_colon'  => __( 'Parent Neighborhoods:', 'varsha' ),
		'not_found'          => __( 'No neighborhoods found.', 'varsha' ),
		'not_found_in_trash' => __( 'No neighborhoods found in Trash.', 'varsha' )
	);

register_post_type( 'neighborhood',
	array(
		'labels'				=> $labels,
		'has_archive'			=> true,
		'hierarchical'			=> true,
		'menu_icon'				=> 'dashicons-admin-home',
		'public'				=> true,
		'rewrite'				=> array( 'slug' => 'neighborhoods', 'with_front' => false ),
		'supports'				=> array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'custom-fields', 'revisions', 'page-attributes' ),
		'taxonomies'			=> array( 'category' ),
	)
);

add_action( 'cmb2_admin_init', 'fls_register_metaboxes' );

function fls_register_metaboxes() {
  $prefix = '_fls_';

  $areas = get_option('mls_areas_options');
  $areas_array = array();
  foreach($areas as $area) {
    $k = $area['redirect'];
    $v = $area['displayLabel'];
    $areas_array[$k] = $v;
  }

  $cmb = new_cmb2_box( array(
    'id'            => $prefix . 'neighborhood_metabox',
    'title'         => __( 'Neighborhood Features', 'cmb2' ),
    'object_types'  => array( 'neighborhood', ),
    'context'    => 'side',
    'priority'   => 'high',
    'show_names' => false, // Show field names on the left
  ) );

  $cmb->add_field( array(
    'name'             => 'Select Custom MLS Area',
    'desc'             => 'Select Custom MLS Area you want affiliated with this neighborhood',
    'id'               => $prefix . 'area',
    'type'             => 'select',
    'show_option_none' => true,
    //'default'          => 'custom',
    'options'          => $areas_array,
  ) );

  $cmb->add_field( array(
    'name'    => 'or Enter search string here',
    'desc'    => 'or Enter a search string here.  This should be the element added to the URL to get the desired properties. i.e., "crescent_h-subdivision/" to return Crescent H properties',
    'id'      => $prefix . 'area_search_string',
    'type'    => 'text',
  ) );

}

function varsha_modify_num_posts_for_neighborhoods($query)
{
    if ($query->is_main_query() && $query->is_post_type_archive('neighborhood') && !is_admin()) {
	    $query->set('posts_per_page', -1);
	    $query->set('post_parent', 0);
    }
}

add_action('pre_get_posts', 'varsha_modify_num_posts_for_neighborhoods');

// Use Single Market Info Template for Posts in Market Info Category
function add_posttype_slug_template( $single_template ){
    if( is_singular( 'post' ) && in_category( 'market-info' ) ){
        $single_template = locate_template( 'single-market-info.php' );
    }
    return $single_template;
}
add_filter( 'single_template', 'add_posttype_slug_template' );

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
}

function tmbr_load_template( $filename, $data = array() ) {
  $file = locate_template( $filename );
  if( $file ){
    extract( $data );
    include( $file );
  }
}
?>
