<?php
/**
 * Register widget areas
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

if ( ! function_exists( 'foundationpress_sidebar_widgets' ) ) :
function foundationpress_sidebar_widgets() {
	register_sidebar(array(
	  'id' => 'above-navigation',
	  'name' => __( 'Above Navigation', 'foundationpress' ),
	  'description' => __( 'Above Menu area.', 'foundationpress' ),
	  'before_widget' => '<article id="%1$s" class="large-12 columns widget %2$s">',
	  'after_widget' => '</article>',
	));
	
	register_sidebar(array(
	  'id' => 'home-top',
	  'name' => __( 'Home Top', 'foundationpress' ),
	  'description' => __( 'Home Top Image container.', 'foundationpress' ),
	  'before_widget' => '<article id="%1$s" class="large-12 columns widget %2$s">',
	  'after_widget' => '</article>',
	));
	
	register_sidebar(array(
	  'id' => 'home-search-bar',
	  'name' => __( 'Home Search Bar', 'foundationpress' ),
	  'description' => __( 'Place search widget here.', 'foundationpress' ),
	  'before_widget' => '<article id="%1$s" class="large-12 columns widget %2$s">',
	  'after_widget' => '</article>',
	));
	
	register_sidebar(array(
	  'id' => 'home-featured',
	  'name' => __( 'Home Featured', 'foundationpress' ),
	  'description' => __( 'Featured Properties area.', 'foundationpress' ),
	  'before_widget' => '<article id="%1$s" class="large-12 columns widget %2$s">',
	  'after_widget' => '</article>',
	  'before_title' => '<h2>',
	  'after_title' => '</h2>',
	));
	
	register_sidebar(array(
	  'id' => 'home-area-info',
	  'name' => __( 'Home Area Info', 'foundationpress' ),
	  'description' => __( 'Get to know Jackson area.', 'foundationpress' ),
	  'before_widget' => '<article id="%1$s" class="columns widget %2$s">',
	  'after_widget' => '</article>',
	  'before_title' => '<h2>',
	  'after_title' => '</h2>',
	));
	
	register_sidebar(array(
	  'id' => 'sidebar-widgets',
	  'name' => __( 'Sidebar widgets', 'foundationpress' ),
	  'description' => __( 'Drag widgets to this sidebar container.', 'foundationpress' ),
	  'before_widget' => '<article id="%1$s" class="widget %2$s">',
	  'after_widget' => '</article>',
	  'before_title' => '<h3>',
	  'after_title' => '</h3>',
	));
	
	register_sidebar(array(
	  'id' => 'sub-footer-1',
	  'name' => __( 'Sub Footer Left', 'foundationpress' ),
	  'description' => __( 'Drag widgets to this sub-footer container', 'foundationpress' ),
	  'before_widget' => '<article id="%1$s" class="large-12 columns widget %2$s">',
	  'after_widget' => '</article>',
	  'before_title' => '<h4>',
	  'after_title' => '</h4>',
	));
	
	register_sidebar(array(
	  'id' => 'sub-footer-2',
	  'name' => __( 'Sub Footer Right', 'foundationpress' ),
	  'description' => __( 'Drag widgets to this sub-footer container', 'foundationpress' ),
	  'before_widget' => '<article id="%1$s" class="large-12 columns widget %2$s">',
	  'after_widget' => '</article>',
	  'before_title' => '<h4>',
	  'after_title' => '</h4>',
	));

	register_sidebar(array(
	  'id' => 'footer-widget-1',
	  'name' => __( 'Footer Widget 1', 'foundationpress' ),
	  'description' => __( 'Drag widgets to this footer container', 'foundationpress' ),
	  'before_widget' => '<article id="%1$s" class="small-12 medium-6 large-3 columns widget %2$s">',
	  'after_widget' => '</article>',
	  'before_title' => '<h5>',
	  'after_title' => '</h5>',
	));
	
	register_sidebar(array(
	  'id' => 'footer-widget-2',
	  'name' => __( 'Footer Widget 2', 'foundationpress' ),
	  'description' => __( 'Drag widgets to this footer container', 'foundationpress' ),
	  'before_widget' => '<article id="%1$s" class="small-12 medium-6 large-3 columns widget %2$s">',
	  'after_widget' => '</article>',
	  'before_title' => '<h5>',
	  'after_title' => '</h5>',
	));
	
	register_sidebar(array(
	  'id' => 'footer-widget-3',
	  'name' => __( 'Footer Widget 3', 'foundationpress' ),
	  'description' => __( 'Drag widgets to this footer container', 'foundationpress' ),
	  'before_widget' => '<article id="%1$s" class="small-12 medium-10 medium-offset-1 large-6 large-offset-0 columns widget %2$s">',
	  'after_widget' => '</article>',
	  'before_title' => '<h5>',
	  'after_title' => '</h5>',
	));
		
	register_sidebar(array(
	  'id' => 'post-footer',
	  'name' => __( 'Post Footer', 'foundationpress' ),
	  'description' => __( 'Drag widgets to this post-footer container', 'foundationpress' ),
	  'before_widget' => '<article id="%1$s" class="widget %2$s">',
	  'after_widget' => '</article>',
	  'before_title' => '<h5>',
	  'after_title' => '</h5>',
	));
}

add_action( 'widgets_init', 'foundationpress_sidebar_widgets' );
endif;
