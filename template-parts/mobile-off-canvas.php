<?php
/**
 * Template part for off canvas menu
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */
?>

<nav class="off-canvas position-right" id="mobile-menu" data-off-canvas data-position="right" role="navigation">
  <?php foundationpress_mobile_nav(); ?>
</nav>

<div class="off-canvas position-left mobile-search"
  id="offCanvasPropertySearch"
  data-off-canvas
  data-position="left"
  >
  <header>
    <p>Property Search</p>
    <span class="dashicons dashicons-no" aria-label="Close menu" data-close></span>
  </header>
  <div class="form-container">
    <?php echo do_shortcode('[mls_search_form]'); ?>
  </div>
</div>

<div class="off-canvas-content" data-off-canvas-content>
