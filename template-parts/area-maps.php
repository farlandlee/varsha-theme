<?php
global $post;
?>
<div class="fullWidth area-map-outer-container">
  <h1 class="area-map-title">Area Map</h1>
  <div class="instructions show-for-small-only">Click on the map to learn more about each area.</div>
  <div class="instructions show-for-medium-only">Click on the map to learn more about each area.</div>
  <div class="area-map-links-container">
    <?php
    if($fields["parent_map_page"] > 0 || $fields["areas"]) {
      if($fields["parent_map_page"] > 0) {
        $parent_fields = get_fields($fields["parent_map_page"]);
        $area_links = $parent_fields["areas"];
        $all_areas_name = $parent_fields["area_name"];
        $all_areas_link = get_the_permalink($fields["parent_map_page"]);
        $all_class = 'map-area-link';
      }
      else {
        $area_links = $fields["areas"];
        $all_areas_name = $fields["area_name"];
        $all_areas_link = get_the_permalink($post->ID);
        $all_class = 'current-map-area-link map-area-link';
      }
      if($area_links && count($area_links) > 0) :
        ?>
        <div class="area-map-links show-for-large">
          <div>View Properties In:</div>
          <a href="<?php
            echo $all_areas_link; ?>" class="<?php
            echo $all_class; ?>"><?php
            echo $all_areas_name; ?></a>
          <?php foreach ( $area_links as $area_link ) :
          	$class = $area_link["linked_page"] == $post->ID? 'current-map-area-link map-area-link':'map-area-link';
          ?>
          <a href="<?php
            echo get_the_permalink($area_link["linked_page"]); ?>" class="<?php
            echo $class; ?>"><?php
            echo $area_link["area_name"] ?></a>
          <?php endforeach; ?>
        </div>
        <?php
      endif;
    }
    if(!empty($fields["map_image"]) || have_rows('areas')) : ?>
      <div id="area-map">
      <?php
      if( !empty($fields["map_image"]) ) {
        ?>
          <img src="<?php
            echo $fields["map_image"]['url']; ?>" alt="<?php
            echo $all_areas_name; ?>" area map" usemap="#jh-map" id="jh-area-map" width="<?php
            echo $fields["map_width"]; ?>" height="<?php
            echo $fields["map_height"]; ?>" >
        <?php
      }
      if( have_rows('areas') ) :
        $dropdown = '';
      ?>
        <map name="jh-map" id="jh-map" >
          <?php while( have_rows('areas') ): the_row();
          $area_name = get_sub_field('area_name');
          $image = get_sub_field('map_image');
          $polygon_coordinates = get_sub_field('polygon_coordinates');
          $link = get_permalink(get_sub_field('linked_page'));
          if(!empty($link) && !empty($area_name)) $dropdown .= '<li><a href="'.$link.'">'.$area_name.'</a></li>';
          if(empty($image) ||
             empty($polygon_coordinates) ||
             empty($link))
          {
            continue;
          }
          ?>
          <area alt="<?php
            echo $area_name; ?>" title="<?php
            echo $area_name; ?>" href="<?php
            echo $link ?>" shape="poly" coords="<?php
            echo $polygon_coordinates; ?>" data-src="<?php
            echo $image['url']; ?>" />
          <?php endwhile; ?>
        </map>
        <?php
        if(!empty($dropdown)) {
          ?>
          <ul class="dropdown menu view-properties-dropdown hide-for-large" data-dropdown-menu data-close-on-click-inside>
            <li><div class="placeholder-text">View Properties In</div>
              <ul class="menu">
                <?php echo $dropdown; ?>
              </ul>
            </li>
          </ul>
          <?php
        }
        ?>
      <?php endif; ?>
      </div><!-- close id=area-map -->
    <?php endif; ?>
  </div>
</div>
