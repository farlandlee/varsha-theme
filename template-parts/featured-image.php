<?php
// If a featured image is set, insert into layout and use Interchange
// to select the optimal image size per named media query.
if ( has_post_thumbnail( $post->ID ) ) : ?>
	<header id="featured-hero" role="banner" data-interchange="[<?php
		echo the_post_thumbnail_url('featured-small'); ?>, small], [<?php
		echo the_post_thumbnail_url('featured-medium'); ?>, medium], [<?php
		echo the_post_thumbnail_url('featured-large'); ?>, large], [<?php
		echo the_post_thumbnail_url('featured-xlarge'); ?>, xlarge]">
	</header>
<?php else : ?>
	<?php
	$default_hero = get_field('default_hero_image', 'option');
	if( !empty($default_hero) ):
	?>
	<header id="featured-hero" role="banner" data-interchange="[<?php
		echo $default_hero['sizes'][ 'featured-small']; ?>, small], [<?php
		echo $default_hero['sizes']['featured-medium']; ?>, medium], [<?php
		echo $default_hero['sizes']['featured-large']; ?>, large], [<?php
		echo $default_hero['sizes']['featured-xlarge']; ?>, xlarge]">
	</header>
	<?php endif; ?>
<?php endif;
