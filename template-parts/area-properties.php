<?php
global $post;
if($fields['search_area'] == '') return;
?>
<div class="area-properties">
  <div class="area-properties-title">Properties available <?php the_title(); ?></div>
  <div class="area-properties-title-arrow"></div>
  <div class="area-properties-inner">
    <h1 id="js-area-view-name" class="area-properties-view-name">Map View</h1>
    <?php
      echo do_shortcode('[mls_show_listings search="/'.
        $fields['search_area'].'-area/"
        view="map"
        min_height="700px"
        sort_top="true"
        per_page_top="false"
        per_page_bottom="false"
        per_page="12"
        view_top="true"
        pagination_top="false"
        pagination_bottom="true"]');
    ?>
  </div>
</div>
