jQuery(function($){

	// Add shrink class to Top Bar after 50px
	$(document).on("scroll", function(){

		if($(document).scrollTop() > 50){
			$("#site-navigation").addClass("shrink");			

		} else {
			$("#site-navigation").removeClass("shrink");			
		}

	});

});