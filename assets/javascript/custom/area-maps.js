function preload(arrayOfImages) {
  jQuery(arrayOfImages).each(function(){
    (new Image()).src = this;
  });
}

jQuery(document).ready(function($) {

	if($('map').length) {
		let $id = $('map').attr('id');
		$('img[usemap]').rwdImageMaps();

		let origSrc = $('#jh-area-map').attr('src');
		$('#' + $id + ' area').hover(
			function(){
				let _src = $(this).data('src');
				let _target = $(this).attr('href');
				$('#jh-area-map').attr('src',_src);
				$('.area-map-links').find('a').removeClass('active-hover');
				$('.area-map-links').find('a[href="' + _target + '"]').addClass('active-hover');
			},
			function() {
				$('#jh-area-map').attr('src',origSrc);
				$('.area-map-links').find('a').removeClass('active-hover');
			}
		);

		$('.area-map-links').find('a').hover(
			function(){
				let _target = $(this).attr('href');
				let _currentArea = $('#' + $id + ' area[href="' + _target + '"]');
				let _src = _currentArea.data('src');
				$('#jh-area-map').attr('src',_src);
				$('.area-map-links').find('a').removeClass('active-hover');
				$(this).addClass('active-hover');
			},
			function() {
				$('#jh-area-map').attr('src',origSrc);
				$('.area-map-links').find('a').removeClass('active-hover');
			}
		);
	}

  if($('#js-area-view-name').length) {
    $('body').on('click','#mls-view-options-tabs a', function(){
      let str = $(this).attr('href');
      let regex = /([^\/]+)-view/g;
      let m = regex.exec(str);
      $('#js-area-view-name').text(m[1]+' View');
    });
  }

	if($('#jh-map').length) {
    let preloadArray = [];
    $('area','#jh-map').each(function(){
      preloadArray.push($(this).data('src'));
    });
		preload(preloadArray);
	}
});
