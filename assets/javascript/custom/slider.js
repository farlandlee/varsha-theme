jQuery(document).ready(function($){
	
	if($('#mls_feed_featured_property_widget-2').length) {
		$('div.mls-featured-property','#mls_feed_featured_property_widget-2').slick({
		    respondTo: 'window',
 		    variableWidth: true,
		    infinite: true,
		    centerMode: true,
		    swipeToSlide: true,
		    centerPadding: '10px',
		    //cssEase: 'ease-in-out',
		    cssEase: 'linear',
		    
		    responsive: [
		    {
		      breakpoint: 400,
		      settings: {
		        variableWidth: false
		      }
		    }]
		});
	}
});