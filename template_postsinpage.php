<!-- NOTE: If you need to make changes to this file, copy it to your current theme's main
	directory so your changes won't be overwritten when the plugin is upgraded. -->

<!-- Start of Post Wrap -->
<div class="post hentry ivycat-post">
	<!-- This is the output of the post TITLE -->
	<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'medium' ); ?></a>
	<h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
	<div class="entry-date"><?php the_date(); ?></div>

	<!-- This is the output of the EXCERPT -->
	<div class="entry-summary">
		<?php the_excerpt(); ?>
	</div>

	
</div>
<!-- // End of Post Wrap -->
