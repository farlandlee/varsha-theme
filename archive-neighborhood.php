<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<header id="featured-hero" role="banner" data-interchange="[<?php echo the_post_thumbnail_url('featured-small'); ?>, small], [<?php echo the_post_thumbnail_url('featured-medium'); ?>, medium], [<?php echo the_post_thumbnail_url('featured-large'); ?>, large], [<?php echo the_post_thumbnail_url('featured-xlarge'); ?>, xlarge]">
</header>

<div class="fullWidth">
	<div class="listings-search-bar">
		<div class="search-bar-outer">
			<div class="search-bar">
				<h2>Property Search</h2>
				<?php dynamic_sidebar( 'home-search-bar' ); ?>
			</div>
		</div>
	</div>
</div>

<div id="page-edge-bleed" role="main">
	<article class="main-content">
		<div class="row">
			<?php
			$intro = varsha_neighborhood_get_option( 'neighborhoods_intro' );
			echo '<div class="neighborhood-intro">';
			echo $intro;
			echo '</div>';
			?>
		</div>
		<div class="row">
			<div class="medium-12 large-3 columns">
				<div class="area-buttons">
					<div class="neighborhood-middle">
						<?php
						$middle = varsha_neighborhood_get_option( 'neighborhoods_middle' );
						echo $middle;
						?>
					</div>
					
					<?php if ( have_posts() ) : ?>
			
					<?php /* Start the Loop */ ?>
			
					<?php while ( have_posts() ) : the_post(); ?>
						<?php get_template_part( 'template-parts/content', 'neighborhoods' ); ?>
					<?php endwhile; ?>
			
					<?php else : ?>
						<?php get_template_part( 'template-parts/content', 'none' ); ?>
			
					<?php endif; // End have_posts() check. ?>
				</div>
			</div>
	
		<div class="medium-12 large-9 columns">
			<div id="varsha-area-map">
				<?php
				$options = get_option('mls_feed_options');
		
				$map_view_template_id = $options['map_view_page_ID'];
		
				echo '<p id="tv"><a href="' . get_permalink( $map_view_template_id ) . '/teton_village-area/">Teton Village</a></p>';
		
				echo '<p id="westbank_north"><a href="' . get_permalink( $map_view_template_id ) . '/westbank_north-area/">West Bank North</a></p>';
		
				echo '<p id="westbank_south"><a href="' . get_permalink( $map_view_template_id ) . '/westbank_south-area/">West Bank South</a></p>';
		
				echo '<p id="north"><a href="' . get_permalink( $map_view_template_id ) . '/north_jackson_hole-area/">North of Jackson</a></p>';
		
				echo '<p id="west_of_jackson"><a href="' . get_permalink( $map_view_template_id ) . '/west_of_jackson-area/">West of Jackson</a></p>';
		
				echo '<p id="jackson"><a href="' . get_permalink( $map_view_template_id ) . '/town_of_jackson-area/">Town of Jackson</a></p>';
		
				echo '<p id="south"><a href="' . get_permalink( $map_view_template_id ) . '/south_jackson_hole-area/">South of Jackson</a></p>';
		
				?>
			</div>
		</div>
		</div>

	<?php
		$outro = varsha_neighborhood_get_option( 'neighborhoods_outro' );
		echo '<div class="neighborhood-outro">';
		echo $outro;
		echo '</div>';
	?>
	</article>

</div>

<?php get_footer();
