<?php
/*
Template Name: Recreation
*/
get_header(); ?>

<?php get_template_part( 'template-parts/featured-image' ); ?>

<div class="fullWidth">
	<div class="listings-search-bar">
		<div class="search-bar-outer">
			<div class="search-bar">
				<h2>Property Search</h2>
				<?php dynamic_sidebar( 'home-search-bar' ); ?>
			</div>
		</div>
	</div>
</div>

<div id="page-full-width" role="main">

<?php do_action( 'foundationpress_before_content' ); ?>
<?php while ( have_posts() ) : the_post(); ?>
  <article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
      <div class="recreation-area">
	      <header>
	          <h1 class="entry-title"><?php the_title(); ?></h1>
	      </header>
	      <?php do_action( 'foundationpress_page_before_entry_content' ); ?>
		  <div class="entry-content">
		      
		          <?php the_content(); ?>
		          
		          <div class="recreation-area-box">
			          <img src="<?php the_field('winter_image'); ?>" alt="Winter in Jackson Hole" class="alignright" />
			          <h4><?php the_field('winter_title'); ?></h4>
			          <p><?php the_field('winter_text'); ?></p>
		          
					  <h5><a class="fancybox-inline" href="#fancyDiv1"><?php the_field('winter_link_title'); ?> &#43;</a></h5>
		          
			          <div style="display:none" class="fancybox-hidden">
						    <div id="fancyDiv1">
							    <?php if( have_rows('winter_links') ): ?>
							    <?php while( have_rows('winter_links') ): the_row(); ?>
							        <div class="resources">
								        <img src="<?php the_sub_field('logoimage'); ?>" class="resource-logo" />
							        
								        <h5>&middot; <?php the_sub_field('resource_name'); ?> &middot; <a href="<?php the_sub_field('resource_link'); ?>" target="_blank"><?php the_sub_field('resource_link_title'); ?></a></h5>
								        <p><?php the_sub_field('description'); ?></p>
							        </div>
						     
									<?php endwhile; ?>
							    <?php endif; ?>
						    </div>
			          </div>
		          </div><!--recreation-area-box-->

		          <div class="recreation-area-box">
			          <img src="<?php the_field('fishing_image'); ?>" alt="Fishing in Jackson Hole" class="alignleft" />
			          <h4><?php the_field('fishing_title'); ?></h4>
			          <p><?php the_field('fishing_text'); ?></p>
		          
		        		         
					<h5><a class="fancybox-inline" href="#fancyDiv2"><?php the_field('fishing_link_title'); ?> &#43;</a></h5>
					    
				    <div style="display:none" class="fancybox-hidden">
					    <div id="fancyDiv2">
						    <?php if( have_rows('fishing_links') ): ?>
						    <?php while( have_rows('fishing_links') ): the_row(); ?>
								<div class="resources">
							        <img src="<?php the_sub_field('logoimage'); ?>" class="resource-logo" />
						        
							        <h5>&middot; <?php the_sub_field('resource_name'); ?> &middot; <a href="<?php the_sub_field('resource_link'); ?>" target="_blank"><?php the_sub_field('resource_link_title'); ?></a></h5>
									<p><?php the_sub_field('description'); ?></p>
					        </div>
							<?php endwhile; ?>
						<?php endif; ?>
					    </div>
				    </div>
				    
				  </div><!--recreation-area-box-->

				  <div class="recreation-area-box">		          
			          <img src="<?php the_field('hiking_image'); ?>" alt="Hiking in Jackson Hole" class="alignright" />
			          <h4><?php the_field('hiking_title'); ?></h4>
			          <p><?php the_field('hiking_text'); ?></p>
			          
			          <?php if( get_field('hiking_link_title') ): ?>		          
			          <h5><a class="fancybox-inline" href="#fancyDiv3"><?php the_field('hiking_link_title'); ?> &#43;</a></h5>
			          <?php endif; ?>
			          
			          <div style="display:none" class="fancybox-hidden">
						<div id="fancyDiv3">
						    <?php if( have_rows('hiking_links') ): ?>
						    <?php while( have_rows('hiking_links') ): the_row(); ?>
						        <div class="resources">
							        <img src="<?php the_sub_field('logoimage'); ?>" class="resource-logo" />
						        
							        <h5>&middot; <?php the_sub_field('resource_name'); ?> &middot; <a href="<?php the_sub_field('resource_link'); ?>" target="_blank"><?php the_sub_field('resource_link_title'); ?></a></h5>
							        <p><?php the_sub_field('description'); ?></p>
						        </div>
						    <?php endwhile; ?>
						<?php endif; ?>
						</div>
			          </div>
			      </div><!--recreation-area-box-->
			      
			      <div class="recreation-area-box">
					  <?php if( get_field('biking_image') ): ?>		          
			          	<img src="<?php the_field('biking_image'); ?>" alt="Biking in Jackson Hole" class="alignleft" />
			          <?php endif; ?>
			          <h4><?php the_field('biking_title'); ?></h4>
			          <p><?php the_field('biking_text'); ?></p>
			          
			          <?php if( get_field('biking_link_title') ): ?>	          
			          	<h5><a class="fancybox-inline" href="#fancyDiv4"><?php the_field('biking_link_title'); ?> &#43;</a></h5>
			          <?php endif; ?>
			          
			          <div style="display:none" class="fancybox-hidden">
						<div id="fancyDiv4">
						    <?php if( have_rows('biking_links') ): ?>
						    <?php while( have_rows('biking_links') ): the_row(); ?>
						        <div class="resources">
							        <img src="<?php the_sub_field('logoimage'); ?>" class="resource-logo" />
						        
							        <h5>&middot; <?php the_sub_field('resource_name'); ?> &middot; <a href="<?php the_sub_field('resource_link'); ?>" target="_blank"><?php the_sub_field('resource_link_title'); ?></a></h5>
							        <p><?php the_sub_field('description'); ?></p>
						        </div>
						    <?php endwhile; ?>
						<?php endif; ?>
						</div>
			          </div>
			      </div><!--recreation-area-box-->	
	
				  <div class="recreation-area-box">
					  <?php if( get_field('golfing_image') ): ?>		          
			          	<img src="<?php the_field('golfing_image'); ?>" alt="Golfing in Jackson Hole" class="alignright" />
			          <?php endif; ?>
			          <h4><?php the_field('golfing_title'); ?></h4>
			          <p><?php the_field('golfing_text'); ?></p>
			          
			          <?php if( get_field('golfing_link_title') ): ?>	          
			          	<h5><a class="fancybox-inline" href="#fancyDiv5"><?php the_field('golfing_link_title'); ?> &#43;</a></h5>
			          <?php endif; ?>
			          
			          <div style="display:none" class="fancybox-hidden">
						<div id="fancyDiv5">
						    <?php if( have_rows('golfing_links') ): ?>
						    <?php while( have_rows('golfing_links') ): the_row(); ?>
						        <div class="resources">
							        <img src="<?php the_sub_field('logoimage'); ?>" class="resource-logo" />
						        
							        <h5>&middot; <?php the_sub_field('resource_name'); ?> &middot; <a href="<?php the_sub_field('resource_link'); ?>" target="_blank"><?php the_sub_field('resource_link_title'); ?></a></h5>
							        <p><?php the_sub_field('description'); ?></p>
						        </div>
						    <?php endwhile; ?>
						<?php endif; ?>
						</div>
			          </div>
			      </div><!--recreation-area-box-->
			      
			      <div class="recreation-area-box">
					  <?php if( get_field('rafting_image') ): ?>		          
			          	<img src="<?php the_field('rafting_image'); ?>" alt="Rafting in Jackson Hole" class="alignleft" />
			          <?php endif; ?>
			          <h4><?php the_field('rafting_title'); ?></h4>
			          <p><?php the_field('rafting_text'); ?></p>
			          
			          <?php if( get_field('rafting_link_title') ): ?>	          
			          	<h5><a class="fancybox-inline" href="#fancyDiv6"><?php the_field('rafting_link_title'); ?> &#43;</a></h5>
			          <?php endif; ?>
			          
			          <div style="display:none" class="fancybox-hidden">
						<div id="fancyDiv6">
						    <?php if( have_rows('rafting_links') ): ?>
						    <?php while( have_rows('rafting_links') ): the_row(); ?>
						        <div class="resources">
							        <img src="<?php the_sub_field('logoimage'); ?>" class="resource-logo" />
						        
							        <h5>&middot; <?php the_sub_field('resource_name'); ?> &middot; <a href="<?php the_sub_field('resource_link'); ?>" target="_blank"><?php the_sub_field('resource_link_title'); ?></a></h5>
							        <p><?php the_sub_field('description'); ?></p>
						        </div>
						    <?php endwhile; ?>
						<?php endif; ?>
						</div>
			          </div>
			      </div><!--recreation-area-box-->
			      
		  </div>
      </div>
      <footer>
          <?php wp_link_pages( array('before' => '<nav id="page-nav"><p>' . __( 'Pages:', 'foundationpress' ), 'after' => '</p></nav>' ) ); ?>
          <p><?php the_tags(); ?></p>
      </footer>
      <?php do_action( 'foundationpress_page_before_comments' ); ?>
      <?php comments_template(); ?>
      <?php do_action( 'foundationpress_page_after_comments' ); ?>
  </article>
<?php endwhile;?>

<?php do_action( 'foundationpress_after_content' ); ?>

</div>

<?php get_footer();
