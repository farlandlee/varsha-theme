<?php
/*
Template Name: Map Page
*/
get_header(); ?>

<?php get_template_part( 'template-parts/featured-image' ); ?>

<div class="fullWidth">
	<div class="listings-search-bar">
		<div class="search-bar-outer">
			<div class="search-bar">
				<h2>Property Search</h2>
				<?php dynamic_sidebar( 'home-search-bar' ); ?>
			</div>
		</div>
	</div>
</div>

<div id="page-screen-width" role="main">

<?php do_action( 'foundationpress_before_content' ); ?>
<?php while ( have_posts() ) : the_post(); ?>
  <header class="main-line">
        <h1 class="entry-title"><span><?php the_title(); ?></span></h1>
  </header>
  <article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
      <?php do_action( 'foundationpress_page_before_entry_content' ); ?>
      <div class="entry-content">
          <?php the_content(); ?>
      </div>
      <footer>
          <?php wp_link_pages( array('before' => '<nav id="page-nav"><p>' . __( 'Pages:', 'foundationpress' ), 'after' => '</p></nav>' ) ); ?>
          <p><?php the_tags(); ?></p>
      </footer>
      <?php do_action( 'foundationpress_page_before_comments' ); ?>
      <?php comments_template(); ?>
      <?php do_action( 'foundationpress_page_after_comments' ); ?>
  </article>
<?php endwhile;?>

<?php do_action( 'foundationpress_after_content' ); ?>

</div>

<?php get_footer();