<?php
/*
Template Name: Area Map
*/
get_header();
$fields = get_fields();
?>

<?php get_template_part( 'template-parts/featured-image' ); ?>

<div class="fullWidth">
	<div class="listings-search-bar">
		<div class="search-bar-outer">
			<div class="search-bar">
				<h2>Property Search</h2>
				<?php dynamic_sidebar( 'home-search-bar' ); ?>
			</div>
		</div>
	</div>
</div>
<?php tmbr_load_template( 'template-parts/area-maps.php', array(
	      'fields' => $fields
	    ) );
?>
<div id="page-full-width" role="main">

<?php do_action( 'foundationpress_before_content' ); ?>
<?php while ( have_posts() ) : the_post(); ?>
  <article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
    <?php do_action( 'foundationpress_page_before_entry_content' ); ?>
    <div class="entry-content">
        <?php the_content(); ?>
    </div>

  </article>

<?php endwhile;?>

<?php do_action( 'foundationpress_after_content' ); ?>

</div>
<?php tmbr_load_template( 'template-parts/area-properties.php', array(
	'fields' => $fields
) ); ?>

<?php get_footer();
